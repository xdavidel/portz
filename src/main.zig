const std = @import("std");
const builtin = @import("builtin");
const net = std.net;
const debug_mode = builtin.mode == std.builtin.OptimizeMode.Debug;

const ArgParseError = error{
    NotEnoughArguments,
    InvalidArgument,
};

const PortStatus = enum {
    Unknown,
    Open,
    Closed,
};

const MAX_PORT: u16 = 65_535;

const arguments_t = struct {
    ipaddr: net.Address,
};

const port_status_t = struct {
    port: u16,
    status: PortStatus,
};

var process_name: ?[:0]const u8 = undefined;

fn usage(err: ArgParseError) void {
    std.debug.print("{any}\n", .{err});
    std.debug.print("usage: {s} Ip4Address\n", .{process_name.?});
}

fn parse_args(args: *std.process.ArgIterator) ArgParseError!arguments_t {
    process_name = args.next();

    var ipstr: [:0]const u8 = undefined;

    if (args.next()) |arg| {
        ipstr = arg;
    } else {
        return ArgParseError.NotEnoughArguments;
    }

    return arguments_t{
        .ipaddr = net.Address.parseIp(ipstr, 0) catch {
            return ArgParseError.InvalidArgument;
        },
    };
}

fn scan(ipaddr: std.net.Address, port_list: []port_status_t) void {
    var addr = ipaddr;
    var port = &port_list[0];

    std.log.debug("scanning port {d}", .{port.port});
    addr.setPort(port.port);
    _ = std.net.tcpConnectToAddress(addr) catch {
        port.status = PortStatus.Closed;
        return;
    };

    std.io.getStdOut().writer().print(".", .{}) catch {};
    port.status = PortStatus.Open;
}

fn print_results() void {}

pub fn main() !void {
    var allocator: std.mem.Allocator = undefined;
    if (debug_mode) {
        var gpa = std.heap.GeneralPurposeAllocator(.{}){};
        allocator = gpa.allocator();
        std.log.debug("Using general purpose allocator", .{});
    } else {
        allocator = std.heap.page_allocator;
        std.log.info("Using page allocator", .{});
    }

    var args = try std.process.argsWithAllocator(allocator);

    const parsed_args = parse_args(&args) catch |err| {
        usage(err);
        std.process.exit(1);
    };

    var wg: std.Thread.WaitGroup = std.Thread.WaitGroup{};
    var pool: std.Thread.Pool = undefined;
    try std.Thread.Pool.init(&pool, .{ .allocator = allocator });
    defer pool.deinit();

    var port_list: [MAX_PORT]port_status_t = undefined;
    for (0.., &port_list) |i, *port| {
        port.status = PortStatus.Unknown;
        port.port = @intCast(i + 1);
    }

    for (0.., port_list) |i, _| {
        pool.spawnWg(&wg, scan, .{ parsed_args.ipaddr, port_list[i..] });
    }
    wg.wait();
    for (port_list) |p| {
        // std.log.debug("Port {any}", .{p});
        if (p.status == PortStatus.Open) {
            try std.io.getStdOut().writer().print("\nPort {d} is open", .{p.port});
        }
    }
}
